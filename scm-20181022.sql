/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100133
Source Host           : 127.0.0.1:3306
Source Database       : scm-20181016

Target Server Type    : MYSQL
Target Server Version : 100133
File Encoding         : 65001

Date: 2018-10-22 13:47:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for estimates
-- ----------------------------
DROP TABLE IF EXISTS `estimates`;
CREATE TABLE `estimates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_number` varchar(20) DEFAULT NULL,
  `supplier_id` int(10) unsigned NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `delivery_code` varchar(5) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `part_number` varchar(50) DEFAULT NULL,
  `note` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `supplier_sku` varchar(20) DEFAULT NULL,
  `provider_code` varchar(10) DEFAULT NULL,
  `manufacturer_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for estimate_log
-- ----------------------------
DROP TABLE IF EXISTS `estimate_log`;
CREATE TABLE `estimate_log` (
  `id` bigint(20) unsigned NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`state_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for procurements
-- ----------------------------
DROP TABLE IF EXISTS `procurements`;
CREATE TABLE `procurements` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sequence` varchar(20) NOT NULL,
  `state_code` varchar(5) DEFAULT NULL,
  `stage` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `executed_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for procurement_items
-- ----------------------------
DROP TABLE IF EXISTS `procurement_items`;
CREATE TABLE `procurement_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `procurement_id` bigint(20) unsigned NOT NULL,
  `product_id` bigint(20) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `note` text,
  `estimate_code` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for purchases
-- ----------------------------
DROP TABLE IF EXISTS `purchases`;
CREATE TABLE `purchases` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_number` varchar(20) DEFAULT NULL,
  `supplier_id` int(10) unsigned NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `supplier_status` varchar(5) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `provider_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for purchase_items
-- ----------------------------
DROP TABLE IF EXISTS `purchase_items`;
CREATE TABLE `purchase_items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` bigint(20) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `state_code` varchar(5) NOT NULL,
  `delivery_code` varchar(5) DEFAULT NULL,
  `shipping_date` date DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `notes` text,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `estimate_id` bigint(20) unsigned DEFAULT NULL,
  `supplier_sku` varchar(20) DEFAULT NULL,
  `supplier_status` varchar(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for purchase_log
-- ----------------------------
DROP TABLE IF EXISTS `purchase_log`;
CREATE TABLE `purchase_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `state_code` varchar(5) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`state_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
