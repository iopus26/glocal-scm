<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/9
 * Time: 上午 01:03
 */

namespace Scm\Core\Constant;


class StateCode
{
    const DRAFT = '000';
    const NEW = '100';
    const QUEUED = '200';
    const EXECUTED = '300';
    const COMPLETED = '400';
    const FINISHED = '500';
    const PENDING = '600';
    const WRONG = '800';
    const CANCELED = '900';

}
