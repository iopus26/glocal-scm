<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/9
 * Time: 上午 01:57
 */

namespace Scm\Core\Contract;


abstract class AbstractProvider
{
    abstract function execute($wrapper);

}
