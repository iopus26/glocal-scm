<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/9
 * Time: 上午 01:57
 */

namespace Scm\Core\Contract;

use Scm\Core\Eloquent\Procurement;

abstract class AbstractHandler
{
    /**
     * @var AbstractHandler
     */
    protected $successor;

    public function setNext( AbstractHandler $successor ){
        $this->successor = $successor;
    }

    abstract public function execute( Procurement $wrapper );


    public function next(Procurement $wrapper){
        if ($this->successor)
            $this->successor->execute($wrapper);
    }

}
