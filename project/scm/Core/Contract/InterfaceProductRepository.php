<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/16
 * Time: 上午 02:24
 */

namespace Scm\Core\Contract;


namespace Scm\Core\Contract;

use Scm\Core\Variable\ProductVariable;

Interface InterfaceProductRepository
{
    /**
     * @param mixed
     * @return ProductVariable
     */
    public static function getProductByKey( $key );

    /**
     * @param $keys
     * @return ProductVariable[]
     */
    public static function getProductByKeys($keys);
}

