<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/14
 * Time: 上午 01:30
 */

namespace Scm\Core\Contract;

use Exception;
use Scm\Core\Constant\ProcurementStateCode;
use Scm\Core\Contract\InterfaceProductRepository;
use Scm\Core\Repository\ProcurementRepository;

abstract class AbstractProcurementManager
{
    var $productRepository;
    public function __construct(InterfaceProductRepository $productRepository){
        $this->productRepository = $productRepository;
    }

    abstract function createSequence();


    abstract function execute();


    /**
     * @param $data
     * @param $state_code
     * @return mixed|\Scm\Core\Eloquent\Procurement
     * @throws Exception
     */
    public function create($data , $state_code = ProcurementStateCode::NEW){
        if (self::checkItemsAvailable($data)){
            $attributes = [];
            $attributes['sequence'] = $this->createSequence();
            $attributes['state_code'] = $state_code;
            $procurement = ProcurementRepository::create($attributes);
            foreach ($data as $key => $quantity){
                $attributes = [];
                $attributes['procurement_id'] = $procurement->id;
                $attributes['product_id'] = $key;
                $attributes['quantity'] = $quantity;
                ProcurementRepository::saveItem($attributes , $procurement);
            }
            $procurement->load('items');
            return $procurement;
        } else {
            throw new Exception('some products not exist.');
        }
    }

    private function checkItemsAvailable($data)
    {
        $ids = array_keys($data);

        $products = $this->productRepository::getProductByKeys($ids);

        if (count($ids) == count($products))
            return true;
        else
            return false;
    }

}
