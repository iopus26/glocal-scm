<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/10/1
 * Time: 下午 01:20
 */

namespace Scm\Core\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

trait PurchaseLoggerTrait
{
    var $table = 'purchase_log';

    public function log($id, $state_code, $content)
    {


        $query = "
insert into $this->table (id , state_code , content , created_at , updated_at) 
values ( :id , :state_code , :content , :created_at , :updated_at)
on duplicate key 
update state_code = :state_code_1 , content = :content_1 , updated_at = :updated_at_1           
        ";

        DB::insert($query, [
            'id' => $id,
            'state_code' => $state_code,
            'content' => $content,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),

            'state_code_1' => $state_code,
            'content_1' => $content,
            'updated_at_1' => Carbon::now()->toDateTimeString()
        ]);


    }

}
