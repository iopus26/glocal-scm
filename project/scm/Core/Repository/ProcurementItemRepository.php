<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/9
 * Time: 上午 12:59
 */

namespace Scm\Core\Repository;


class ProcurementItemRepository
{
    public static function updateEstimateId( $item , $estimate_id)
    {
        $item->estimate_id = $estimate_id;
        $item->save();
    }
}
