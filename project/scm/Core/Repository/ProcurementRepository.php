<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/9
 * Time: 上午 12:59
 */

namespace Scm\Core\Repository;

use Scm\Core\Constant\ProcurementStateCode as StateCode;
use Scm\Core\Eloquent\Procurement;
use Scm\Core\Eloquent\ProcurementItem;

class ProcurementRepository
{
    /**
     * @param $attributes
     * @return Procurement|mixed
     */
    public static function create($attributes ){
        $procurement = Procurement::create($attributes);
        return $procurement;
    }

    public static function saveItem($attributes , $procurement){
        $item = new ProcurementItem($attributes);
        $procurement->items()->save($item);
        return $item;
    }

    public static function get(){
        $procurements = Procurement::with('items')->whereIn( 'state_code' , [StateCode::NEW])->get();

        return $procurements;
    }


    public static function getHandleable(){

        return Procurement::whereIn('state_code' , [ StateCode::NEW , StateCode::ESTIMATING , StateCode::ESTIMATED, StateCode::PURCHASING,StateCode::PURCHASED , StateCode::EXECUTED , StateCode::PENDING])->get();
    }

}
