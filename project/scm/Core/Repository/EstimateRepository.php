<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/9/3
 * Time: 下午 02:51
 */

namespace Scm\Core\Repository;

use Scm\Core\Constant\StateCode;
use Scm\Core\Eloquent\Estimate;

class EstimateRepository
{

    /**
     * @param $attributes []
     * @return mixed
     */
    public static function create($attributes)
    {
        $estimate = Estimate::create($attributes);
        return $estimate;
    }

    public static function getHandleable( $procurement_id ){

        return Estimate::whereIn('state_code' , [ StateCode::NEW , StateCode::QUEUED , StateCode::EXECUTED])
            ->join('procurement_items' , 'estimates.id','procurement_items.estimate_id' )
            ->where('procurement_items.procurement_id' ,$procurement_id)
            ->select('estimates.*')
            ->get();
    }

    public static function get( $procurement_id ){

        return Estimate::join('procurement_items' , 'estimates.id','procurement_items.estimate_id' )
            ->where('procurement_items.procurement_id' ,$procurement_id)
            ->select('estimates.*')
            ->get();
    }
}
