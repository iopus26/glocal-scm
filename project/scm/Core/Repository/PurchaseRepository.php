<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/9/3
 * Time: 下午 02:51
 */

namespace Scm\Core\Repository;

use Scm\Core\Eloquent\Purchase;

class PurchaseRepository
{

    /**
     * @param $attributes []
     * @return mixed
     */
    public static function create($attributes)
    {
        $purchase = Purchase::create($attributes);
        return $purchase;
    }


}
