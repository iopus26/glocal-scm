<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/8
 * Time: 下午 11:12
 */

namespace Scm\Core\Driver;


use Scm\Core\Driver\Contract\CurlDriver;
use Scm\Core\Variable\CurlResponse;

class JapanDriver extends CurlDriver
{
    var $response;
    public function execute()
    {
        $response = new CurlResponse();
        $response->request = $this->options;

        $time_start = microtime(true);
        $ch = curl_init();
        curl_setopt_array($ch, $this->options);
        $response->setResponse(mb_convert_encoding(curl_exec($ch), "UTF-8", "Shift_JIS"));


        $response->setErrorNumber(curl_errno($ch));
        $response->setHttpCode(@curl_getinfo($ch, CURLINFO_HTTP_CODE));

        curl_close($ch);
        $time_end = microtime(true);
        $response->setTime($time_end - $time_start);
        $this->response = $response;
        return $response;

    }
}
