<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/8
 * Time: 下午 11:08
 */

namespace Scm\Core\Driver\Contract;


interface DriverInterface
{
    public function __construct();
    public function execute();
}
