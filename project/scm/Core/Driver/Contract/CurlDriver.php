<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/8
 * Time: 下午 11:09
 */

namespace Scm\Core\Driver\Contract;


abstract class CurlDriver implements DriverInterface
{
    const CURLOPT_CONNECTTIMEOUT = 60;
    const CURLOPT_TIMEOUT = 60;

    protected $header = [];
    protected $options = [];
    protected $response;
    private $_options = [];

    public function __construct()
    {
        $this->setHeaders();
        $this->setOptions();
        $this->makeOptions();
    }

    function setHeaders()
    {
        $this->header[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
        $this->header[] = "Connection: keep-alive";
        $this->header[] = "Accept-Charset: Shift_JIS,utf-8;q=0.7,*;q=0.3";
        $this->header[] = "Accept-Language: ja,en-US;q=0.8,en;q=0.6";
        $this->header[] = "Pragma: ";
    }

    function setOptions()
    {
        $this->_options[CURLOPT_ENCODING] = 'gzip,deflate,sdch';
        $this->_options[CURLOPT_FOLLOWLOCATION] = true;
        $this->_options[CURLOPT_CONNECTTIMEOUT] = self::CURLOPT_CONNECTTIMEOUT;
        $this->_options[CURLOPT_TIMEOUT] = self::CURLOPT_TIMEOUT;
        $this->_options[CURLOPT_RETURNTRANSFER] = true;
        $this->_options[CURLOPT_USERAGENT] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64)';
    }

    public function appendHeader($value)
    {
        $this->header[] = $value;
        $this->makeOptions();
    }

    public function appendOption($key, $value)
    {
        $this->_options[$key] = $value;
        $this->makeOptions();
    }

    function makeOptions()
    {
        $this->options = $this->_options;
        $this->options[CURLOPT_HTTPHEADER] = $this->header;
    }
}
