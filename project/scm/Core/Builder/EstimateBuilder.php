<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/10
 * Time: 下午 11:28
 */

namespace Scm\Core\Builder;

use Scm\Core\Constant\StateCode;
use Scm\Core\Contract\InterfaceProductRepository;
use Scm\Core\Eloquent\Procurement;
use Scm\Core\Repository\EstimateRepository;
use Scm\Core\Variable\ProductVariable;

class EstimateBuilder
{
    /**
     * @param InterfaceProductRepository $productRepository
     * @param $procurement Procurement
     * @param string $state
     */
    public static function create(InterfaceProductRepository $productRepository , $procurement, $state = StateCode::NEW)
    {
        foreach ( $procurement->items as $item ){
            $product = $productRepository::getProductByKey($item->product_id);
            $quantity = $item->quantity;
            $variable = new ProductVariable($product, $quantity);
            $estimate = self::make($variable,$state);
            $item->estimate()->save($estimate);
        }

    }

    /**
     * @param $variable ProductVariable
     * @return mixed
     */
    private static function make($variable , $state){
        $attributes = [];
        $attributes['supplier_id'] = $variable->supplier_id;
        if ($variable->type == 1){
            $attributes['manufacturer_id'] = $variable->manufacturer_id;
            $attributes['part_number'] = $variable->name;
        }

        $attributes['state_code'] = $state;
        $attributes['sku'] = $variable->sku;
        $attributes['quantity'] = $variable->quantity;
        $estimate = EstimateRepository::create($attributes);
        return $estimate;


    }
}
