<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/10
 * Time: 下午 11:28
 */

namespace Scm\Core\Builder;

use Scm\Core\Constant\ProcurementStateCode;
use Scm\Core\Constant\StateCode;
use Scm\Core\Eloquent\Procurement;
use Scm\Core\Repository\PurchaseRepository;

class PurchaseBuilder
{
    /**
     * @param $procurement Procurement
     * @param string $state
     */
    public static function create($procurement, $state = StateCode::NEW)
    {
        $items = $procurement->items()->with('estimate')->get();
        $estimates= [];
        foreach ($items as $item){
            $estimates[] = $item->estimate;
        }
        $estimates = collect($estimates);

        $groups = $estimates->groupBy('supplier_id');

        foreach ($groups as $key => $group){
            $attributes = [];
            $attributes['supplier_id'] = $key;
            $attributes['parent_id'] = $procurement->id;
            $attributes['state_code'] = $state;
            $purchase = PurchaseRepository::create($attributes);
            foreach ($group as $estimate){

                $attributes = [];
                $attributes['sku'] = $estimate->sku;
                $attributes['estimate_id'] = $estimate->id;
                $attributes['quantity'] = $estimate->quantity;
                $attributes['state_code'] = StateCode::NEW;
                $attributes['parent_id'] = $estimate->parent_id;
                $attributes['supplier_sku'] = $estimate->supplier_sku;
                $purchase->items()->create($attributes);
            }
        }

        $procurement->state_code = ProcurementStateCode::PURCHASING;
        $procurement->stage = 'purchase';
        $procurement->save();

    }


}
