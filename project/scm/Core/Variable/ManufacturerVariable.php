<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/14
 * Time: 上午 12:53
 */

namespace Scm\Core\Variable;


class ManufacturerVariable
{
    public $id;
    public $name;
    public function __construct( $id , $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}
