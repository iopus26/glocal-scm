<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/10
 * Time: 下午 10:55
 */

namespace Scm\Core\Variable;


class EstimateResponse
{
    public $delivery_code;
    public $part_number;
    public $cost;
}
