<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/9
 * Time: 上午 01:31
 */

namespace Scm\Core\Variable;


class ProductVariable
{
    public $id;
    public $name;
    public $sku;
    public $manufacturer_id;
    public $manufacturer_name;
    public $supplier_id;
    public $quantity;
    public $type;

    public function __construct( $product , $quantity = 0)
    {

        $this->id = $product->id;
        $this->name = $product->name;
        $this->sku = $product->sku;
        $this->manufacturer_name = $product->manufacturer_name;
        $this->manufacturer_id = $product->manufacturer_id;
        $this->supplier_id = $product->supplier_id;
        $this->quantity = $quantity;
        $this->type = $product->type;

    }
}
