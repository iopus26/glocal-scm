<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/8/27
 * Time: 下午 10:44
 */

namespace Scm\Core\Variable;


class CurlResponse
{
    var $error_number;
    var $response;
    var $http_code;
    var $time;
    var $request;

    public function setRequest( $value ){
        $this->request = $value;
    }

    public function getRequest(){
        return $this->request;
    }

    public function getTime(){
        return $this->time;
    }
    public function setTime( $value ){
        $this->time = $value;
    }

    public function getHttpCode(){
        return $this->http_code;
    }
    public function setHttpCode( $value ){
        $this->http_code = $value;
    }

    public function getErrorNumber(){
        return $this->error_number;
    }
    public function setErrorNumber( $value ){
        $this->error_number = $value;
    }

    public function getResponse(){
        return $this->response;
    }
    public function setResponse( $value ){
        $this->response = $value;
    }
}
