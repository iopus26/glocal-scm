<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/9/4
 * Time: 上午 11:24
 */

namespace Scm\Core\Adapter\Japan\Api;


use Scm\Core\Driver\JapanDriver;

class RegularEstimateStatusApi
{
    var $url = 'http://www.webike.net/wbs/eg-order-select.html';
    var $fields = [];
    protected $driver;
    var $response;

    public function __construct($code)
    {
        $this->fields['codes'] = $code;
    }

    public function execute()
    {
        $this->response = null;
        $this->driver = new JapanDriver();
        $this->driver->appendOption(CURLOPT_URL, $this->url);
        $this->driver->appendOption(CURLOPT_POSTFIELDS, http_build_query($this->fields));
        $this->response = $this->driver->execute();
        return $this->response ;

    }

    public function getResponse()
    {
        return $this->response;
    }

}
