<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/11
 * Time: 上午 03:10
 */

namespace Scm\Core\Adapter\Japan\Api;

use Illuminate\Support\Collection;
use Scm\Core\Driver\JapanDriver;
use Scm\Core\Eloquent\PurchaseItem;

class PurchaseRequestApi
{
    protected $driver;
    var $url = 'https://www.webike.net/wbs/eg-order.html';
    var $fields = [];
    var $response;

    /**
     * PurchaseRequestApi constructor.
     * @param PurchaseItem[]|Collection $purchaseItems
     */
    public function __construct(  $purchaseItems )
    {
        $this->fields['userId'] = env('RCJ_KAIIN_ID');
        $this->fields['shopCode'] = env('RCJ_SHOP_CODE');
        $this->fields['products'] = [];
        foreach ($purchaseItems as $item){
            $this->fields['products'][] = [
                'sku' => $item->supplier_sku ?: $item->sku ,
                'num' => $item->quantity,
            ];

        }
    }


    public function execute()
    {
        $this->response = null;
        $post_data['order'] = $this->fields;

        $this->driver = new JapanDriver();
        $this->driver->appendHeader('Content-Type: application/json');
        $this->driver->appendHeader('Accept: application/json');

        $this->driver->appendOption(CURLOPT_URL, $this->url);
        $this->driver->appendOption(CURLOPT_POSTFIELDS, json_encode($post_data));

        $this->response = $this->driver->execute();
        return $this->response;
    }

    public function getTransactionNumber()
    {
        if ($this->response->getHttpCode() == 200 ){
            $response = json_decode($this->response->getResponse());
            if (!isset($response->response->errors)){
                return $response->response->orderId;
            }
        }
        return null;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
