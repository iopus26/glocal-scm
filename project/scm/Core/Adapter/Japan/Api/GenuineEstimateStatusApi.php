<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/9/4
 * Time: 上午 11:24
 */

namespace Scm\Core\Adapter\Japan\Api;

use Scm\Core\Driver\JapanDriver;
use Scm\Core\Variable\CurlResponse;

class GenuineEstimateStatusApi
{
    var $url = 'http://www.webike.net/wbs/genuine-estimate-status.html';
    var $fields = [];
    var $driver;
    /**
     * @var CurlResponse
     */
    var $response;

    public function __construct($code)
    {
        $this->fields['user_id'] = env('RCJ_KAIIN_ID');
        $this->fields['estimate_id'] = $code;

    }

    public function execute()
    {
        $this->response = null;

        $this->driver = new JapanDriver();
        $this->driver->appendOption(CURLOPT_URL, $this->url);
        $this->driver->appendOption(CURLOPT_POSTFIELDS, http_build_query($this->fields));

        $this->response = $this->driver->execute();
        dump($this->response);
        return $this->response;

    }


    public function getInfo()
    {

        if ($this->response->getHttpCode() == 200) {
            $response = json_decode($this->response->getResponse());
            $attributes = [];
            if (!$response->error_message && $response->estimate_done == 1) {
                foreach ($response->estimate_info as $meisai) {
                    $attribute = [
                        'key' => 'part_number',
                        'part_number' => $meisai->syouhin_code,
                        'cost' => $meisai->syouhin_kakaku_2,
                        'delivery_code' => $this->deliveryConvert($meisai),
                        'stock_qty' => $meisai->webike_stock_cnt,
                        'qty' => $meisai->mitumorizokusei_syouhin_num
                    ];

                    $attributes[] = $attribute;
                }
                return $attributes;
            }

        }
        return null;
    }

    function deliveryConvert($meisai)
    {
        if (is_numeric($meisai->delivery_time))
            return str_pad($meisai->delivery_time, 3, "0", STR_PAD_LEFT);
        else
            return $meisai->syouhin_nouki_code;
    }


    public function getResponse()
    {
        return $this->response;
    }
}
