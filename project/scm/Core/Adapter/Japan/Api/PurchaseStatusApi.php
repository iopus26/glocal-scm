<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/11
 * Time: 上午 03:10
 */

namespace Scm\Core\Adapter\Japan\Api;

use Scm\Core\Constant\StateCode;
use Scm\Core\Driver\JapanDriver;

class PurchaseStatusApi
{
    var $url = 'http://www.webike.net/wbs/eg-order-select.html';
    var $fields = [];
    protected $driver;
    var $response;

    public function __construct($code)
    {
        $this->fields['codes'] = $code;
    }

    public function execute()
    {
        $this->response = null;
        $this->driver = new JapanDriver();
        $this->driver->appendOption(CURLOPT_URL, $this->url);
        $this->driver->appendOption(CURLOPT_POSTFIELDS, http_build_query($this->fields));
        $this->response = $this->driver->execute();
        return $this->response ;

    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getSupplierStatus(){
        $response = json_decode($this->getResponse()->response)->response;
        if (count($response) ){
            return $response[0]->tyumon_status;
        }
        return null;
    }

    public function getShippingDate(){
        $response = json_decode($this->getResponse()->response)->response;
        if (count($response) ){
            return $response[0]->shippingDate;
        }
        return null;
    }

    public function getItemSupplierStatus($sku){
        $response = json_decode($this->getResponse()->response)->response;
        if (count($response) ){
            $meisais = $response[0]->meisai;
            foreach ($meisais as $meisai){
                if ($meisai->meisai_syouhin_sys_code === $sku){
                    return $meisai->meisai_status_code;
                }
            }
        }
        return null;
    }

    public function getItemShippingDate($sku){
        $response = json_decode($this->getResponse()->response)->response;
        if (count($response) ){
            $meisais = $response[0]->meisai;
            foreach ($meisais as $meisai){
                if ($meisai->meisai_syouhin_sys_code === $sku){
                    return $meisai->meisai_nyuuka_yotei;
                }
            }
        }
        return null;
    }

}
