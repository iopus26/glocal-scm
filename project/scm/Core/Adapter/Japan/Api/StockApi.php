<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/8
 * Time: 下午 11:15
 */

namespace Scm\Core\Adapter\Japan\Api;

use Scm\Core\Driver\JapanDriver;

class StockApi
{
    var $url = 'http://www.webike.net/wbs/stock-cooperation-sd-json.html';
    var $sku;
    var $driver;
    var $response;

    public function __construct( $sku )
    {
        $this->sku = $sku;
    }

    public function execute()
    {
        $this->response = null;
        $this->driver = new JapanDriver();
        $this->driver->appendOption(CURLOPT_URL, $this->url);
        $this->driver->appendOption(CURLOPT_POSTFIELDS, http_build_query(['sku' => $this->sku]));
        $this->response = $this->driver->execute();
        return $this->response ;
    }
    public function getResponse()
    {
        return $this->response;
    }
}
