<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/9/4
 * Time: 上午 11:24
 */

namespace Scm\Core\Adapter\Japan\Api;

use Scm\Core\Driver\JapanDriver;
use Scm\Core\Variable\CurlResponse;

class GenuineEstimateRequestApi
{
    var $driver;
    var $url;
    var $maker;
    var $part_number;
    /**
     * @var CurlResponse
     */
    var $response;
    var $quantity;
    public function __construct($maker , $part_number , $quantity)
    {
        $this->url = 'http://www.webike.net/api/genuine_parts/estimate_request.json?shop_code=' . env('RCJ_SHOP_CODE')  . '&kaiin_id=' . env('RCJ_KAIIN_ID') . '&forcibly_flag=1';
        $this->maker = $maker;
        $this->part_number = $part_number;
        $this->quantity = $quantity;
    }

    public function execute()
    {
        $this->response = null;

        $args = '';

        $args .= sprintf("&product_code=%s&quantity=%s" , $this->part_number, $this->quantity);

        $url = $this->url . '&maker_name=' . $this->maker . $args  ;

        if (in_array($this->maker , ['HONDA','SUZUKI','YAMAHA','KAWASAKI']))
            $url .= '&realtime_flag=1';

        $this->driver = new JapanDriver();
        $this->driver->appendHeader('Content-Type: application/json');
        $this->driver->appendHeader('Accept: application/json');
        $this->driver->appendOption(CURLOPT_URL, $url);

        $this->response = $this->driver->execute();
        return $this->response;
    }

    public function getTransactionNumber()
    {

        if ($this->response->getHttpCode() == 201 ){
            $response = json_decode($this->response->getResponse());
            if (!$response->error_message){
                return $response->estimate_id;
            }
        }
        return null;
    }


    public function getResponse()
    {
        return $this->response;
    }
}
