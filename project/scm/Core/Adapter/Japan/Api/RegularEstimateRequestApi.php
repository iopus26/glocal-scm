<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/9/4
 * Time: 上午 11:24
 */

namespace Scm\Core\Adapter\Japan\Api;

use Scm\Core\Driver\JapanDriver;

class RegularEstimateRequestApi
{
    protected $driver;
    var $url = 'http://www.webike.net/wbs/eg-order-estimate.html';
    var $fields = [];
    var $response;

    public function __construct( $sku , $quantity)
    {
        $this->fields['userId'] = env('RCJ_KAIIN_ID');
        $this->fields['shopCode'] = env('RCJ_SHOP_CODE');
        $this->fields['products'] = [];
        $this->fields['products'][] = ['sku' => $sku , 'num' => $quantity];

    }


    public function execute()
    {
        exit();
        $this->response = null;
        $post_data['order'] = $this->fields;
        $this->driver = new JapanDriver();
        $this->driver->appendHeader('Content-Type: application/json');
        $this->driver->appendHeader('Accept: application/json');

        $this->driver->appendOption(CURLOPT_URL, $this->url);
        $this->driver->appendOption(CURLOPT_POSTFIELDS, json_encode($post_data));

        $this->response = $this->driver->execute();
        return $this->response;

    }


    public function getTransactionNumber()
    {

        if ($this->response->getHttpCode() == 200 ){
            $response = json_decode($this->response->getResponse());
            if (!isset($response->response->errors)){
                return $response->response->orderId;
            }
        }
        return null;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
