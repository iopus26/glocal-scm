<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/14
 * Time: 下午 06:57
 */

namespace Scm\Core\Adapter\Japan;


use Scm\Core\Adapter\Japan\Api\PurchaseRequestApi;
use Scm\Core\Adapter\Japan\Api\PurchaseStatusApi;
use Scm\Core\Eloquent\Purchase;
use Scm\Core\Traits\PurchaseLoggerTrait;

class PurchaseAdapter
{
    use PurchaseLoggerTrait;

    /**
     * @param Purchase $purchase
     * @return string | null
     */
    function makeRequest($purchase){
        $api = new PurchaseRequestApi($purchase->items()->get());
        // TODO below code not test yet.
        return null;
        $api->execute();
        $this->log($purchase->id,$purchase->state_code , json_encode($api->getResponse()));
        if ($transactionNumber = $api->getTransactionNumber()){
            return $transactionNumber;
        }
        return null;
    }

    /**
     * @param $purchase
     * @return array|false
     */
    function getStatus($purchase){
        $api = new PurchaseStatusApi($purchase->transaction_number);
        $api->execute();
        $this->log($purchase->id,$purchase->state_code , json_encode($api->getResponse()));
        $supplierStatus =  $api->getSupplierStatus();


        if ($supplierStatus){
            $attributes = [];
            $attributes['purchase']['supplier_status'] = $supplierStatus;
            if ($shippingDate = $api->getShippingDate())
                $attributes['purchase']['shipping_date'] = $shippingDate;


            foreach ($purchase->items as $item){
                $attributes['item'][$item->id]['supplier_status'] = $api->getItemSupplierStatus($item->supplier_sku ? : $item->sku);
                $attributes['item'][$item->id]['shipping_date']= $api->getItemShippingDate($item->supplier_sku ? : $item->sku);
            }

            return $attributes;
        }
        return false;


    }
}
