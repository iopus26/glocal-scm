<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/10
 * Time: 下午 09:38
 */

namespace Scm\Core\Adapter\Japan;

use Scm\Core\Adapter\Japan\Api\RegularEstimateRequestApi;
use Scm\Core\Adapter\Japan\Api\RegularEstimateStatusApi;
use Scm\Core\Adapter\Japan\Api\StockApi;
use Scm\Core\Traits\EstimateLoggerTrait;
use Scm\Core\Eloquent\Estimate;

class EstimateRegularAdapter
{
    use EstimateLoggerTrait;
    /**
     * @param Estimate $estimate
     * @return array|false
     */
    function inquireStock(Estimate $estimate){
        $api = new StockApi($estimate->sku);
        $response = json_decode($api->execute()->response);
        $this->log($estimate->id,$estimate->state_code , json_encode($api->getResponse()));
        $attributes = [];

        if (!count($response)){
            $attributes['delivery_code'] = 'A20';
        } else {
            $response = $response[0];
            if ($response->soldout_flg){
                $attributes['delivery_code'] = 'A10';
            } else {

                if( $estimate->quantity <= $response->webike_stock){
                    $attributes['delivery_code'] = '000';
                    $attributes['supplier_sku'] = $response->sku_code;
                } else {
                    return false;
                }

            }
        }

        return $attributes;

    }

    /**
     * @param Estimate $estimate
     * @return string | false
     */
    function makeRequest(Estimate $estimate){
        $api = new RegularEstimateRequestApi($estimate->sku , $estimate->quantity);
        $api->execute();
        $this->log($estimate->id,$estimate->state_code , json_encode($api->getResponse()));
        if ($transactionNumber = $api->getTransactionNumber()){
            return $transactionNumber;
        }
        return false;
    }

    /**
     * @param Estimate $estimate
     * @return array|false
     */
    function getStatus(Estimate $estimate){
        $api = new RegularEstimateStatusApi($estimate->transaction_number);
        $api->execute();
        $this->log($estimate->id,$estimate->state_code , json_encode($api->getResponse()));
        $response = json_decode($api->getResponse()->response)->response;
        $attributes = [];
        if (count($response) && $response[0]->tyumon_status == '81'){
            $attributes['delivery_code'] = $response[0]->meisai[0]->calc_noukistatus;
            $attributes['supplier_sku'] = $response[0]->meisai[0]->meisai_syouhin_sys_code;
            return $attributes;
        }

        return false;

    }


}
