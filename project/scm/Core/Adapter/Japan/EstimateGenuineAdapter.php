<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/10
 * Time: 下午 09:39
 */

namespace Scm\Core\Adapter\Japan;

use Scm\Core\Adapter\Japan\Api\GenuineEstimateRequestApi;
use Scm\Core\Adapter\Japan\Api\GenuineEstimateStatusApi;
use Scm\Core\Traits\EstimateLoggerTrait;
use Scm\Core\Eloquent\Estimate;

class EstimateGenuineAdapter
{
    use EstimateLoggerTrait;

    /**
     * @param Estimate $estimate
     * @param string $manufacturer_name
     * @return string|false
     */
    function makeRequest(Estimate $estimate , $manufacturer_name){
        $api = new GenuineEstimateRequestApi($manufacturer_name, $estimate->part_number , $estimate->quantity);
        $api->execute();
        $this->log($estimate->id,$estimate->state_code , json_encode($api->getResponse()));
        if ($transactionNumber = $api->getTransactionNumber()){
            return $transactionNumber;
        }
        return false;
    }

    /**
     * @param $estimate
     * @return array|false
     */
    function getStatus($estimate){
        $api = new GenuineEstimateStatusApi($estimate->transaction_number);
        $api->execute();
        $this->log($estimate->id,$estimate->state_code , json_encode($api->getResponse()));
        $response = json_decode($api->getResponse()->response);
        if (isset($response->estimate_info) && count($response->estimate_info)){
            $info = $response->estimate_info[0];
            $attributes = [];
            if (is_numeric($info->delivery_time)){
                $attributes['delivery_code'] = $info->delivery_time;
                $attributes['supplier_sku'] = $info->syouhin_sys_code;
            }
            else {
                $attributes['delivery_code'] = $info->syouhin_nouki_code;
            }
            return $attributes;
        }
        return false;
    }
}
