<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/9/3
 * Time: 上午 08:03
 */

namespace Scm\Core\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * Scm\Core\Eloquent\PurchaseItem
 *
 * @property int $id
 * @property int $purchase_id
 * @property string $sku
 * @property string $state_code
 * @property string|null $delivery_code
 * @property string|null $shipping_date
 * @property int $quantity
 * @property string|null $notes
 * @property int|null $parent_id
 * @property int|null $estimate_id
 * @property string|null $supplier_sku
 * @property string|null $supplier_status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereDeliveryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereEstimateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem wherePurchaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereShippingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereStateCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereSupplierSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereSupplierStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\PurchaseItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PurchaseItem extends Model
{
    protected $guarded = [];
}
