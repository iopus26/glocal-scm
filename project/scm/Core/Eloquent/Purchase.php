<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/9/3
 * Time: 上午 08:03
 */

namespace Scm\Core\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * Scm\Core\Eloquent\Purchase
 *
 * @property int $id
 * @property string|null $transaction_number
 * @property int $supplier_id
 * @property string $state_code
 * @property int|null $parent_id
 * @property string|null $supplier_status
 * @property string|null $shipping_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $provider_code
 * @property-read \Illuminate\Database\Eloquent\Collection|\Scm\Core\Eloquent\PurchaseItem[] $items
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereProviderCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereShippingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereStateCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereSupplierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereSupplierStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereTransactionNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Purchase whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Purchase extends Model
{
    protected $guarded = [];

    public function items(){
        return $this->hasMany(PurchaseItem::class);
    }
}
