<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/9/3
 * Time: 上午 08:03
 */

namespace Scm\Core\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * Scm\Core\Eloquent\Procurement
 *
 * @property int $id
 * @property string $sequence
 * @property string|null $state_code
 * @property string|null $stage
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $executed_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Scm\Core\Eloquent\Estimate[] $estimates
 * @property-read \Illuminate\Database\Eloquent\Collection|\Scm\Core\Eloquent\ProcurementItem[] $items
 * @property-read \Illuminate\Database\Eloquent\Collection|\Scm\Core\Eloquent\Purchase[] $purchases
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Procurement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Procurement whereExecutedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Procurement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Procurement whereSequence($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Procurement whereStage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Procurement whereStateCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Procurement whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Procurement extends Model
{
    protected $guarded = [];

    public function items(){
        return $this->hasMany(ProcurementItem::class);
    }

    public function estimates(){
        return $this->hasManyThrough(
            Estimate::class,
            ProcurementItem::class,
            'procurement_id',
            'parent_id',
            'id',
            'id'
        );
    }

    public function purchases(){
        return $this->hasManyThrough(
            Purchase::class,
            ProcurementItem::class,
            'procurement_id',
            'parent_id',
            'id',
            'id'
        );
    }

}
