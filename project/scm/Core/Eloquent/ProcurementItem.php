<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/9
 * Time: 上午 01:00
 */

namespace Scm\Core\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * Scm\Core\Eloquent\ProcurementItem
 *
 * @property int $id
 * @property int $procurement_id
 * @property int $product_id
 * @property int $quantity
 * @property string|null $note
 * @property string|null $estimate_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Scm\Core\Eloquent\Estimate $estimate
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\ProcurementItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\ProcurementItem whereEstimateCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\ProcurementItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\ProcurementItem whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\ProcurementItem whereProcurementId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\ProcurementItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\ProcurementItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\ProcurementItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ProcurementItem extends Model
{
    protected $guarded = [];

    public function estimate(){
        return $this->hasOne(Estimate::class , 'parent_id' , 'id');
    }
}
