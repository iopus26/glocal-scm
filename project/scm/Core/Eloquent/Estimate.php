<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/9/3
 * Time: 上午 08:03
 */

namespace Scm\Core\Eloquent;


use Illuminate\Database\Eloquent\Model;

/**
 * Scm\Core\Eloquent\Estimate
 *
 * @property int $id
 * @property string|null $transaction_number
 * @property int $supplier_id
 * @property string $state_code
 * @property string $sku
 * @property string|null $delivery_code
 * @property float|null $cost
 * @property int $quantity
 * @property string|null $part_number
 * @property string|null $note
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $parent_id
 * @property string|null $supplier_sku
 * @property string|null $provider_code
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereDeliveryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate wherePartNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereProviderCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereStateCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereSupplierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereSupplierSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereTransactionNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $manufacturer_id
 * @method static \Illuminate\Database\Eloquent\Builder|\Scm\Core\Eloquent\Estimate whereManufacturerId($value)
 */
class Estimate extends Model
{
    protected $guarded = [];

}
