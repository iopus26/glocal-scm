<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/14
 * Time: 上午 01:28
 */

namespace Scm\Local;

use Scm\Core\Contract\AbstractProcurementManager;
use Scm\Core\Repository\ProcurementRepository;
use Scm\Local\Chain\Client;


class ProcurementManager extends AbstractProcurementManager
{


    /**
     * create unique sequence for each procurement
     *
     * @return int
     */
    function createSequence()
    {
        return time();
    }

    /**
     * execute all handleable procurement of each iteration
     */
    function execute()
    {
        $procurements = ProcurementRepository::getHandleable();
        foreach ($procurements as $procurement){
            $client = new Client();
            $client->execute($procurement);
        }
    }
}
