<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/9
 * Time: 上午 02:00
 */

namespace Scm\Local\Chain;

use Scm\Core\Builder\EstimateBuilder;
use Scm\Core\Constant\ProcurementStateCode;
use Scm\Core\Contract\AbstractHandler;
use Scm\Core\Eloquent\Procurement;
use Scm\Local\Repository\ProductRepository;

class NewHandler extends AbstractHandler
{

    public function execute(Procurement $wrapper)
    {
        if ($wrapper->state_code == ProcurementStateCode::NEW) {
            $this->process($wrapper);
        }

        $this->next($wrapper);
    }

    private function process( Procurement $wrapper )
    {
        EstimateBuilder::create(new ProductRepository() , $wrapper);
        $wrapper->update(['state_code' => ProcurementStateCode::ESTIMATING , 'stage'=>'estimate']);
    }


}
