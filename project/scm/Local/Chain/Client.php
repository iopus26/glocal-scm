<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/16
 * Time: 上午 02:45
 */

namespace Scm\Local\Chain;


use Scm\Core\Eloquent\Procurement;

class Client
{

    public function execute( Procurement $procurement){
        $newHandler = new NewHandler();
        $estimatingHandler = new EstimatingHandler();
        $estimatedHandler = new EstimatedHandler();
        $purchasingHandler = new PurchasingHandler();
        $purchasedHandler = new PurchasedHandler();


        $newHandler->setNext($estimatingHandler);
        $estimatingHandler->setNext($estimatedHandler);
        $estimatedHandler->setNext($purchasingHandler);
        $purchasingHandler->setNext($purchasedHandler);

        $newHandler->execute($procurement);

    }
}
