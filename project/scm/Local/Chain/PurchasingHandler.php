<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/10/16
 * Time: 上午 11:23
 */

namespace Scm\Local\Chain;


use Scm\Core\Constant\ProcurementStateCode;
use Scm\Core\Constant\StateCode;
use Scm\Core\Contract\AbstractHandler;
use Scm\Core\Eloquent\Procurement;
use Scm\Local\Util\ProviderSelector;

class PurchasingHandler extends AbstractHandler
{
    public function execute(Procurement $wrapper)
    {
        if ($wrapper->state_code == ProcurementStateCode::PURCHASING) {
            $this->process($wrapper);
        }

        $this->next($wrapper);
    }


    private function process(Procurement $wrapper){
        $purchases = $wrapper->purchases()->get();
        foreach ($purchases as $purchase) {
            $provider = ProviderSelector::choosePurchaseProvider($purchase);
            $provider->execute($purchase);
        }

        if (count($purchases->where('state_code', StateCode::EXECUTED)) == count($purchases)) {
            $wrapper->state_code = ProcurementStateCode::PURCHASED;
            $wrapper->save();
            return;
        }
    }
}
