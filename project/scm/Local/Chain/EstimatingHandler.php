<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/11
 * Time: 上午 12:33
 */

namespace Scm\Local\Chain;

use Scm\Core\Constant\ProcurementStateCode;
use Scm\Core\Contract\AbstractHandler;
use Scm\Core\Eloquent\Procurement;
use Scm\Local\Util\ProviderSelector;


class EstimatingHandler extends AbstractHandler
{
    var $estimateProviderSelector;
    var $wrapper;


    public function execute(Procurement $wrapper)
    {
        if ($wrapper->state_code == ProcurementStateCode::ESTIMATING) {
            $this->process($wrapper);
        }

        $this->next($wrapper);
    }

    private function process(Procurement $wrapper)
    {
        $estimates = $wrapper->estimates()->get();
        foreach ($estimates as $estimate){
            $provider = ProviderSelector::chooseEstimateProvider($estimate);
            $provider->execute($estimate);

        }

    }


}
