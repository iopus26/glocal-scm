<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/16
 * Time: 上午 03:23
 */

namespace Scm\Local\Provider\Japan;

use Scm\Core\Adapter\Japan\EstimateGenuineAdapter as JapanEstimateGenuineAdapter;
use Scm\Core\Adapter\Japan\EstimateRegularAdapter as JapanEstimateRegularAdapter;
use Scm\Core\Constant\StateCode;
use Scm\Core\Contract\AbstractProvider;
use Scm\Core\Eloquent\Estimate;
use Scm\Local\Repository\ManufacturerRepository;

class EstimateProvider extends AbstractProvider
{

    /**
     * @param Estimate $estimate
     */
    public function execute($estimate)
    {
        if ($estimate->manufacturer_id) {
            // for rcj genuine parts
            $adapter = new JapanEstimateGenuineAdapter();
        } else {
            // for rcj regular product
            $adapter = new JapanEstimateRegularAdapter();

        }


        if ($estimate->state_code == StateCode::NEW) {
            $result = false;
            if (is_a($adapter, JapanEstimateRegularAdapter::class) &&
                !$this->estimateRequire($adapter, $estimate)
            ) {
                // check rcj stock api . if instock we don't need estimate
                return;
            } elseif (is_a($adapter, JapanEstimateRegularAdapter::class)) {
                // make estimate request for regular product
                $result = $adapter->makeRequest($estimate);
            } elseif (is_a($adapter, JapanEstimateGenuineAdapter::class)) {
                // make estimate request for genuine parts
                $manufacturer = ManufacturerRepository::find($estimate->manufacturer_id);
                $result = $adapter->makeRequest($estimate, $manufacturer->name);
            }

            if ($result !== false) {
                $attributes = [
                    'transaction_number' => $result,
                    'state_code' => StateCode::QUEUED
                ];
                $estimate->update($attributes);
                return;
            }
            return;
        }


        if ($estimate->state_code == StateCode::QUEUED) {
            $result = $adapter->getStatus($estimate);

            if ($result !== false) {
                if (is_a($adapter, JapanEstimateRegularAdapter::class)) {
                    $result['state_code'] = StateCode::EXECUTED;
                } elseif (is_a($adapter, JapanEstimateGenuineAdapter::class)) {
                    if (is_numeric($result['delivery_code'])){
                        $result['state_code'] = StateCode::EXECUTED;
                    } else {
                        $result['state_code'] = StateCode::WRONG;
                    }
                }
                $estimate->update($result);
            }
        }


    }

    /**
     * @param JapanEstimateRegularAdapter $adapter
     * @param Estimate $estimate
     * @return bool
     */
    private function estimateRequire($adapter, $estimate)
    {

        $result = $adapter->inquireStock($estimate);

        if ($result !== false) {
            if ($result['delivery_code'] == 'A20') {
                $result['state_code'] = StateCode::WRONG;
                $estimate->update($result);
                return false;
            } elseif ($result['delivery_code'] == 'A10') {
                $result['state_code'] = StateCode::WRONG;
                $estimate->update($result);
                return false;
            } elseif ($result['delivery_code'] == '000') {
                $result['state_code'] = StateCode::EXECUTED;
                $estimate->update($result);
                return false;
            }

        }
        return true;
    }
}
