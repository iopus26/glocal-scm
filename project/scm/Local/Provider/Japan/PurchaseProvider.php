<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/16
 * Time: 上午 03:23
 */

namespace Scm\Local\Provider\Japan;

use Scm\Core\Adapter\Japan\PurchaseAdapter as JapanPurchaseAdapter;
use Scm\Core\Constant\StateCode;
use Scm\Core\Contract\AbstractProvider;
use Scm\Core\Eloquent\Purchase;

class PurchaseProvider extends AbstractProvider
{

    /**
     * @param Purchase $purchase
     */
    public function execute($purchase)
    {
        $adapter = new JapanPurchaseAdapter();

        if ($purchase->state_code == StateCode::NEW) {
            // make purchase request for all products
            $result = $adapter->makeRequest($purchase);
            if ($result) {
                $attributes = [
                    'transaction_number' => $result,
                    'state_code' => StateCode::QUEUED
                ];
                $purchase->update($attributes);
            }

        } elseif (StateCode::QUEUED == $purchase->state_code) {
            $result = $adapter->getStatus($purchase);
            if ($result !== false) {
                $attributes = $result['purchase'];
                if ($attributes['supplier_status'] == '81') {
                    $attributes['state_code'] = StateCode::EXECUTED;
                }
                $purchase->update($attributes);
                foreach ($purchase->items as $item) {
                    if (isset($result['item'][$item->id])) {
                        $attributes = $result['item'][$item->id];
                        $item->update($attributes);
                    }

                }
            }
        }
    }

}
