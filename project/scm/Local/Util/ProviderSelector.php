<?php
/**
 * Created by PhpStorm.
 * User: User1
 * Date: 2018/10/16
 * Time: 上午 11:25
 */

namespace Scm\Local\Util;

use Scm\Core\Contract\AbstractProvider;
use Scm\Core\Eloquent\Purchase;
use Scm\Local\Provider\Japan\EstimateProvider as JapanEstimateProvider;
use Scm\Local\Provider\Japan\PurchaseProvider as JapanPurchaseProvider;
use Scm\Core\Eloquent\Estimate;

class ProviderSelector
{
    /**
     * @param Estimate $estimate
     * @return AbstractProvider
     * @throws \Exception
     */
    public static function chooseEstimateProvider(Estimate $estimate){
        if ($estimate->supplier_id == 1 )
            return new JapanEstimateProvider();


        throw new \Exception('no provider matched');
    }

    /**
     * @param Purchase $purchase
     * @return AbstractProvider
     * @throws \Exception
     */
    public static function choosePurchaseProvider(Purchase $purchase){
        if ($purchase->supplier_id == 1 )
            return new JapanPurchaseProvider();


        throw new \Exception('no provider matched');
    }
}
