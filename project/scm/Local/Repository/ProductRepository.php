<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/14
 * Time: 上午 12:59
 */

namespace Scm\Local\Repository;


use Scm\Core\Contract\InterfaceProductRepository;
use Scm\Core\Variable\ProductVariable;
use Illuminate\Support\Facades\DB;

class ProductRepository implements InterfaceProductRepository
{

    public static function getProductByKey($id){

        $query = "
select
  products.id,
  products.name,
  products.sku,
  products.category_id,
  manufacturers.name as manufacturer_name,
  manufacturers.id as manufacturer_id,
  suppliers.id as supplier_id,
  suppliers.name as supplier_name,
  type
from
  products
inner join 
  manufacturers
on 
  products.manufacturer_id = manufacturers.id
inner join 
  product_suppliers
on 
  products.id = product_suppliers.product_id
inner join 
  suppliers 
on 
  suppliers.id = product_suppliers.supplier_id
  
where products.id = ? 
limit 1;      
        ";


        $result = DB::connection('products')->select($query , [ $id ]);
        if (count($result))
            return new ProductVariable($result[0]);
        else
            return null;
    }

    public static function getProductByKeys( $ids ){

        $_ids = implode("," , $ids);
        $query = "
select
  products.id,
  products.name,
  products.sku,
  products.category_id,
  manufacturers.name as manufacturer_name,
  manufacturers.id as manufacturer_id,
  suppliers.id as supplier_id,
  suppliers.name as supplier_name,
  type
from
  products
inner join 
  manufacturers
on 
  products.manufacturer_id = manufacturers.id
inner join 
  product_suppliers
on 
  products.id = product_suppliers.product_id
inner join 
  suppliers 
on 
  suppliers.id = product_suppliers.supplier_id
  
where products.id in ( $_ids )       
        ";


        $result = DB::connection('products')->select($query);
        $products = [];
        foreach ($result as $item){
            $products[] = new ProductVariable($item);
        }
        return $products;

    }
}
