<?php
/**
 * Created by PhpStorm.
 * User: iopus
 * Date: 2018/10/14
 * Time: 上午 12:59
 */

namespace Scm\Local\Repository;


use Scm\Core\Contract\InterfaceManufacturerRepository;
use Scm\Core\Variable\ManufacturerVariable;
use Illuminate\Support\Facades\DB;

class ManufacturerRepository
{

    /**
     * @param $key string
     * @return mixed ManufacturerVariable
     */
    public static function find($key)
    {
        if (!$key)
            return null;
        $query ="
select
  id,name
from
  manufacturers 
where id = $key  
limit 1
        ";

        $result = DB::connection('products')->select($query);

        if (!$result)
            return null;


        return new ManufacturerVariable($result[0]->id , $result[0]->name);
    }
}
