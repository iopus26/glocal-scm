Glocal SCM Package
========================================

# Usage
- Copy .env.example => .env & Modify 
 > DB_* => scm project database
 > DB_PRODUCT_* => for ProductRepository maybe you don't need that
 > RCJ_SHOP_CODE & RCJ_KAIIN_ID  authorize information for RCJ API 

- Modify Scm\Local\Repository\ManufacturerRepository
  > Provide manufacturer information 
  
- Modify Scm\Local\Repository\ProductRepository  
  > Provide Product information  
  
- Modify Scm\Local\Util\AutoPurchaseRule
  > Make your rule for auto purchase after estimate job completed  
  > for example : we should check customer paid for his order. 
 
- Modify Scm\Local\Chain\PurchasedHandler
  > Make your business logic after purchased  
  > for example : you should notice the order system that package shipping date has be confirmed.
  
Test Create Procurement  
\> vendor/bin/phpunit --testsuite Create  

Test Execute Procurement  
\> vendor/bin/phpunit --testsuite Execute   

## Workflow
> 1. create procurement job via ProcurementManager.create
> 2. create estimate jobs for the procurement via NewHandler
> 3. request estimate to supplier via  EstimatingHandler
> 4. inquire estimate status from supplier and make purchase job via EstimatedHandler 
> 5. request purchase to supplier and get status via PurchasingHandler
> 6. add what you want to do ...



### Scm\Local\Util\ProviderSelector
> Each supplier will has different api , so we should tell system which api should be use for each product.
   
### Scm\Local\Provider\Japan\EstimateProvider
> Estimate Provider for RCJ
   
### Scm\Local\Provider\Japan\PurchaseProvider
> Purchase Provider for RCJ






