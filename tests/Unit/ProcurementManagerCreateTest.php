<?php

namespace Tests\Unit;


use Scm\Core\Constant\ProcurementStateCode;
use Scm\Core\Eloquent\Procurement;
use Scm\Local\ProcurementManager;
use Scm\Local\Repository\ProductRepository;
use Tests\TestCase;


class ProcurementManagerCreateTest extends TestCase
{
    public function testCreateTest()
    {

        $products = [
            "2408142" => 1,
            '10207972' => 1
            // sku => qty
        ];

        $manager = new ProcurementManager(
            new ProductRepository()
        );

        $procurement = $manager->create( $products , ProcurementStateCode::NEW);

        $this->assertTrue(
            get_class($procurement) == Procurement::class &&
            count($procurement->items)
        );
    }
}
